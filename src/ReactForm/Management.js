import React, { Component } from 'react'
import ListSv from './ListSv';

export default class Management extends Component {
    state = {
        values:{
        id:"",
        name:"",
        numberPhone:"",
        mail:"",
        },
        err:{
            id:"",
        name:"",
        numberPhone:"",
        mail:"",
        },
        listSv:[
          
        ],
        editSv:null,
       searchSv:[]
    }
    handleOnchange =(event)=>{ 
            const {id,value} = event.target;
            const newValue = {...this.state.values,[id]:value}
            const newErr = {...this.state.err}
            if(!value.trim()){
                 newErr[id] = "Không Để Trống"

            }
            else{
                newErr[id] = ""
            }
            this.setState({values:newValue,err:newErr})

         
               
    }
    handleOnBlur=(event)=>{ const {id,value} = event.target;
    const newValue = {...this.state.values,[id]:value}
    const newErr = {...this.state.err}
    if(!value.trim()){
         newErr[id] = "Không Để Trống"

    }
    else{
        newErr[id] = ""
    }
    this.setState({values:newValue,err:newErr})}
    handleSubmit = (event)=>{
        event.preventDefault()
        
        if(!this.formValid()){
            const newSv = [...this.state.listSv,this.state.values]
            this.setState({listSv:newSv,searchSv:newSv})

        }
        
        

        
    }
    formValid = ()=>{
        let inValid = false
        Object.values(this.state.values).forEach((value)=>{
            if(!value){
                inValid = true
            }

        })
        return inValid
        
    }
    handleDelete = (id)=>{
        const indexSv = this.state.listSv.findIndex((item)=>{
           return item.id ===id

        })
        if(indexSv !== -1){
            const newList = [...this.state.listSv]
            newList.splice(indexSv,1)
            this.setState({listSv:newList,
            searchSv:newList
            })
        }


    }
    handleEdit = (id)=>{
const indexSv = this.state.listSv.find((item)=>{

    return item.id === id
});
this.setState({editSv:indexSv,values:indexSv})


    }
    handleUpdate = (event)=>{
        event.preventDefault()
        const indexSv = this.state.listSv.findIndex((item)=>{
   return item.id === this.state.editSv.id
        })
        const newListSv =[...this.state.listSv]
        newListSv[indexSv] = this.state.values
        this.setState({listSv:newListSv,values:{
            id:"",
            name:"",
            numberPhone:"",
            mail:"",
            },
        editSv:null,
        searchSv:newListSv
        })



    }
    handleSearch = (event)=>{
        event.preventDefault()
let Sv = this.state.listSv.filter(item=>item.name.toLowerCase().includes(event.target.value))

this.setState({
searchSv:Sv


})
    }
  render() {
    let {values,err,listSv,editSv,searchSv} = this.state
    let {id,name,numberPhone,mail} = values
  
    return (
      <div className='container'>
        <h4 className='bg-black text-light'>Thông tin sinh viên</h4>
     <form >
     <div className='row' onSubmit={this.handleSubmit}>
       <div className='col-6 p-3'>
      <div className='form-input'>
        <label htmlFor="">Mã Sinh Viên</label>
        <input value={id} type="text" id='id' name='id' disabled={editSv}  className='form-control border-dark' onChange={this.handleOnchange} onBlur={this.handleOnBlur}/>
        {err.id && (
            <span>{err.id}</span>
        )}
      </div>
       </div>
       <div className='col-6 p-3'>
      <div className='form-input'>
        <label htmlFor="" >Họ Tên</label>
        <input value={name} type="text" id='name' name='name' className='form-control border-dark ' onChange={this.handleOnchange} onBlur={this.handleOnBlur}
         />
         {err.name && (
            <span>{err.name}</span>
        )}
      </div>
       </div>
       <div className='col-6 p-3'>
      <div className='form-input'>
        <label htmlFor="">Số Điện Thoại</label>
        <input value={numberPhone} type="text" id='numberPhone' name='numberPhone' className='form-control border-dark'onChange={this.handleOnchange} onBlur={this.handleOnBlur} />
        {err.numberPhone && (
            <span>{err.numberPhone}</span>
        )}
      </div>
       </div>
       <div className='col-6 p-3'>
      <div className='form-input'>
        <label htmlFor="">Email</label>
        <input value={mail} type="text" id='mail' name='mail' className='form-control border-dark' onChange={this.handleOnchange} onBlur={this.handleOnBlur}/>
        {err.mail && (
            <span>{err.mail}</span>
        )}
      </div>
       </div >

       <div className='col-2 mt-3' >
        
        {editSv?(<button className='btn btn-success' onClick={this.handleUpdate}>Cập Nhật</button>):(

<button className='btn btn-primary' onClick={this.handleSubmit} >Thêm Sinh Viên</button>
)}
</div>
        


     </div>
   
     </form>

    


  <div className='mt-4'>
    <form >
    <div className="input-group mb-3">
  <input type="text" className="form-control" onChange={this.handleSearch} placeholder="Search..." />
  
</div>

    </form>
  </div>
  <ListSv dataSearch = {searchSv} data={listSv} handleDelete = {this.handleDelete} handleEdit = {this.handleEdit}/>
      </div>
    )
  }
}
