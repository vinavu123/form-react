import React, { Component } from 'react'

export default class ListSv extends Component {
  render() {
    const {data,handleDelete,handleEdit,dataSearch}=this.props
    return (
      <div className='container'>
        <table className="table">
            <thead >
                <tr>
                    <th>Mã Sv</th>
                    <th>Họ và tên</th>
                    <th>Số Điện Thoại</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody  >
            {dataSearch.map((item,index)=>{
                return(
                
                    <tr key={index}>
                        <td scope="row">{item.id}</td>
                        <td>{item.name}</td>
                        <td>{item.numberPhone}</td>
                        <td>{item.mail}</td>
                        <td><button onClick={()=>{handleEdit(item.id)}}>Edit</button>
                        <button onClick={()=>{handleDelete(item.id)}}>Delete</button>
                        </td>
                    </tr>
                    
                )
            }
            
                
            )}
            </tbody>
           
        </table>
      </div>
    )
  }
}
